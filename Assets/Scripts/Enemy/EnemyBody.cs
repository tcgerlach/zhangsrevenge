﻿using UnityEngine;
using System.Collections;

public class EnemyBody : MonoBehaviour {

	private Enemy enemy;

	void Start() {
		enemy = this.transform.parent.gameObject.GetComponent<Enemy> ();
	}

	void OnTriggerEnter (Collider other) {
		if (other.tag == GameController.PlayerTag) {
			Hero player = other.GetComponent<Hero> ();
			if (player != null) {
				player.OnHitByEnemy ();
			}
		} else if (other.tag == GameController.WallTag || other.tag == GameController.EnemyTag) {
			if (enemy as AdvancingEnemy) {
				((AdvancingEnemy)enemy).ChangeDirection ();
			}
		}
	}
}
