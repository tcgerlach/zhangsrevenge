﻿using UnityEngine;
using System.Collections;

public class EnemyHead : MonoBehaviour {

	private Enemy enemy;

	void Start() {
		enemy = this.transform.parent.gameObject.GetComponent<Enemy> ();
	}

	public void OnCharacterLandedOn () {
		enemy.OnEnemyKilled();
	}
}
