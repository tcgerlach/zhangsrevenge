﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	[SerializeField] private AudioClip EnemyKilledSound;

	public void OnEnemyKilled () {
		AudioManager.Instance.PlayEffect(EnemyKilledSound);
		Destroy (this.gameObject);
		GameController.Instance.IncrementScore ();
	}
}
