﻿using UnityEngine;
using System.Collections;

public class AdvancingEnemy : Enemy {

	private const float MovementSpeed = -1f;
	private const float MovementDistance = 10;

	private bool movementStarted = false;
	private Transform player;
	private Transform enemy;
	private float direction = 1;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag(GameController.PlayerTag).GetComponent<Transform>();
		enemy = this.transform;
	}

	// Update is called once per frame
	void Update () {
		if (!movementStarted) {
			// is the player close enough to start moving?
			if (player.position.x + MovementDistance >= this.transform.position.x) {
				movementStarted = true;
			} 
		}

		if (movementStarted) {
			float newX = enemy.position.x + (MovementSpeed * Time.deltaTime) * direction;
			enemy.position = new Vector3 (newX, enemy.position.y, enemy.position.z);
		}
	}

	public void ChangeDirection() {
		direction *= -1;
		// change direction enemy is facing
		Vector3 scale = new Vector3(enemy.localScale.x * -1, enemy.localScale.y, enemy.localScale.z);
		this.transform.localScale = scale;
	}
}
