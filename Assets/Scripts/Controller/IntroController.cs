﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroController : MonoBehaviour {

	public void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			StartGame ();
		}
	}

	private void StartGame () {
		SceneManager.LoadScene(1);
	}
}
