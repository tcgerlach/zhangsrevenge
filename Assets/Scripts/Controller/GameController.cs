﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	// tags for various object types
	public const string PlayerTag = "Player";
	public const string EnemyTag = "Enemy";
	public const string WallTag = "Wall";
	public const string MovingFloorTag = "MovingFloor";

	// use a HUD prefab so HUD changes are easily propogated to boards
	[SerializeField] private GameObject HUD;
	[SerializeField] private bool IsDungeon;

	// data for audio playing
	[SerializeField] private AudioCatalog SongCatalog; 
	private AudioClip[] BackgroundAudio;
	private AudioSource Player;
	private int AudioIndex = 0;

	// static reference to this object
	public static GameController Instance;

	// These HUD objects loaded by name
	private Text timer;
	private Text score;
	private Text lives;
	private Text pause;
	private Text board;

	// timer info
	private float startTime;
	private bool paused = false;

	void Awake() {
		Instance = this;

		// create the HUD
		// this is done on instantiate so it is created
		// before calls to find the hud in other classes
		Instantiate(HUD);
	}

	void Start () {
		// disable curser
		Cursor.visible = false;

		// get audio source
		Player = this.GetComponent<AudioSource> ();

		// set start time
		startTime = Time.time;

		// update mute flag
		Player.mute = !GameData.Instance.AudioEnabled;

		// get HUD fields
		timer = GameObject.Find ("Timer").GetComponent<Text> ();
		score = GameObject.Find ("Score").GetComponent<Text> ();
		lives = GameObject.Find ("Lives").GetComponent<Text> ();
		pause = GameObject.Find ("Paused").GetComponent<Text> ();
		board = GameObject.Find ("BoardID").GetComponent<Text> (); 

		// the default color scheme is to dark for dungeon boards
		if (IsDungeon) {
			timer.color = Color.yellow;
			score.color = Color.yellow;
			lives.color = Color.yellow;

			board.color = Color.yellow;
		}

		// update HUD
		UpdateHud ();

		// load data from XML
		LevelData Level = LevelDataHandler.Instance.GetLevelData (SceneManager.GetActiveScene ().buildIndex);
		board.text = Level.DisplayName;

		// set background music from config file
		BackgroundAudio = new AudioClip[Level.BackgroundSongs.Length];
		for (int i = 0; i < Level.BackgroundSongs.Length; ++i) {
			BackgroundAudio[i] = SongCatalog.BackgroundSongs[Level.BackgroundSongs[i]];
		}
	}

	private void PlayBackgroundAudio () {
		if (!Player.isPlaying && AudioIndex < BackgroundAudio.Length) {
			Player.clip = BackgroundAudio [AudioIndex];
			Player.Play ();
			AudioIndex = AudioIndex + 1;
		}
		if (AudioIndex == BackgroundAudio.Length) {
			AudioIndex = 0;
		}
     }

	void Update () {
		// pause function
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (paused) {
				Time.timeScale = 1;
			} else {
				Time.timeScale = 0;
			}
			paused = !paused;
			Cursor.visible = paused;
			pause.enabled = paused;
		}

		// audio mute
		if (Input.GetKeyDown (KeyCode.M)) {
			GameData.Instance.AudioEnabled = !GameData.Instance.AudioEnabled;
			Player.mute = !GameData.Instance.AudioEnabled;
		}

		PlayBackgroundAudio ();

		float timeRemaining = (300 - Mathf.Round (Time.time - startTime));
		if (timeRemaining <= 0) {
			LostLife (); 
		}
		timer.text = timeRemaining + "";
	}

	public void IncrementScore() {
		++GameData.Instance.Score;
		UpdateHud();
	}

	public void UpdateHud () {
		score.text = "Score: " + GameData.Instance.Score;
		lives.text = "Lives : " + GameData.Instance.Lives;
	}

	public void LostLife () {
		UpdateHud();
		if (GameData.Instance.Lives == 0) {
			RestartGame ();
		} else {
			GameData.Instance.Lives--;
			int scene = SceneManager.GetActiveScene().buildIndex;
			SceneManager.LoadScene(scene);
		}
	}

	public void RestartGame() {
		GameData.Instance.Score = 0;
		GameData.Instance.Lives = 3;
		startTime = Time.time;
		SceneManager.LoadScene (0);
	}
}
