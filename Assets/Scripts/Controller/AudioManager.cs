﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

	[SerializeField] AudioSource Audio;

	public static AudioManager Instance;

	void Awake() {
		Instance = this;
	}

	public void PlayEffect (AudioClip effect) {
		if (effect != null && GameData.Instance.AudioEnabled) {
			Audio.PlayOneShot (effect);
		}
	}
}
