﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData {
	public int LevelId;
	public string DisplayName;
	public int[] BackgroundSongs;
	public int NextLevel;
	public int WarpLevel;
}
