﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour {

	// handle singleton
	private static GameData instance;
	public static GameData Instance {
		get {
			if (instance == null) {
				GameObject o = new GameObject();
				instance = o.AddComponent<GameData>();

				// set default values
				instance.Score = 0;
				instance.Lives = 3;		
				instance.AudioEnabled = true;
			}
			return instance;
		}
	}

	// ensure this object is never created elsewhere
	void Awake() {
		if ((instance) && (instance.GetInstanceID() != GetInstanceID())) {
			DestroyImmediate(gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
	}

	// data to persist
	public int Score;
	public int Lives;
	public bool AudioEnabled;
}
