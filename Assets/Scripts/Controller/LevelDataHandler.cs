﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;

public class LevelDataHandler : MonoBehaviour {

	private static LevelDataHandler instance;
	public static LevelDataHandler Instance {
		get {
			if (instance == null) {
				GameObject o = new GameObject();
				instance = o.AddComponent<LevelDataHandler>();
			}
			return instance;
		}
	}

	// ensure this object is never created elsewhere
	void Awake() {
		if ((instance) && (instance.GetInstanceID() != GetInstanceID())) {
			DestroyImmediate(gameObject);
		} else {
			instance = this;
			DontDestroyOnLoad(gameObject);
			Init();
		}
	}

    // Sample XML
    //<levels>
    //  <level>
    //    <levelid>1</levelid>
    //	  <displayname>World 1</displayname>
    //	  <audio>
    //      <song>1</song>
    //      <song>2<song>
    //    </audio>
    //	  <nextlevel>2</levelid>
    //  </level>
    //</levels>

    private Dictionary<int,LevelData> LevelsById;

    void Init () {
		// map of level data by id
		LevelsById = new Dictionary<int,LevelData> ();

		// create and load XML level document
		XmlDocument LevelDoc = new XmlDocument ();
		TextAsset xmlFile = Resources.Load ("Levels") as TextAsset;    // Resources/Levels.xml
		LevelDoc.LoadXml (xmlFile.text);                               // load document from xml text

		// load the list of level nodes from document
		XmlNodeList LevelList = LevelDoc.GetElementsByTagName ("level");
		Debug.Log (LevelList.Count + " levels loaded");          

		// create LevelData for each level
		foreach (XmlNode LevelNode in LevelList) { 
			LevelData Level = new LevelData ();

			Level.LevelId = int.Parse (LevelNode.SelectSingleNode ("levelid").InnerText);
			Level.NextLevel = int.Parse (LevelNode.SelectSingleNode ("nextlevel").InnerText);
			Level.WarpLevel = int.Parse (LevelNode.SelectSingleNode ("warplevel").InnerText);
			Level.DisplayName = LevelNode.SelectSingleNode ("displayname").InnerText;

			// get song data
			XmlNode Songs = LevelNode.SelectSingleNode ("audio");
			Level.BackgroundSongs = new int[Songs.SelectNodes("song").Count];
			int songIndex = 0;
			foreach (XmlNode Song in Songs.SelectNodes("song")) {
				Level.BackgroundSongs[songIndex++] = int.Parse(Song.InnerText);
			}

			LevelsById.Add(Level.LevelId, Level);
        }
    }

    // Load a level
    public LevelData GetLevelData(int LevelId) {
        return LevelsById[LevelId];	
    }
}