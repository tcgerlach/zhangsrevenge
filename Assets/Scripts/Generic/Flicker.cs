﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour {

	private const float FLICKER_RATE = .5f;
	private const float FLICKER_SCALE = .9f;

	private bool isLarge = true;
	private Vector3 largeScale;
	private Vector3 smallScale;
	private Transform t;

	// Use this for initialization
	void Start () {
		InvokeRepeating("Rescale", FLICKER_RATE, FLICKER_RATE);
		t = this.transform;
		largeScale = t.localScale;
		smallScale = largeScale * FLICKER_SCALE;
	}
	
	private void Rescale() {
		if (isLarge) {
			t.localScale = smallScale;
		} else {
			t.localScale = largeScale;
		}
		isLarge = !isLarge;
	}
}
