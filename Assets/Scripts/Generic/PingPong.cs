﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PingPong : MonoBehaviour {

	// cached transform
	private Transform myTransform;

	// direction to move
	public Vector3 MoveDir = Vector3.zero;
	
	// speed of movement in units / second
	public float Speed = 0.0f;
	
	// distance to travel
	public float TravelDistance = 0.0f;
	
	// Use this for initialization
	IEnumerator Start () {
		myTransform = transform;

		// loop forever
		while (true) {
			MoveDir *= -1;	// change direction
			yield return StartCoroutine(Travel());	// move transform
		}
	}
	
	IEnumerator Travel() {
		float traveled = 0;
		while (traveled < TravelDistance) {
			Vector3 distToTravel = MoveDir * Speed * Time.deltaTime;
			myTransform.position += distToTravel;
			traveled += distToTravel.magnitude;
			yield return null;
		}
	}
}
