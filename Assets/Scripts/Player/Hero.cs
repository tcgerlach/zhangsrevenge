﻿using UnityEngine;
using System.Collections;

// NOTES:
// Moving platforms must be tagged as MovingFloor
// OnCharacterHitFromBelow sent to objects when hero's head hits them
// OnCharacterLandedOn sent to objects the player lands on
// OnCharacterLeft sent to objects when the player is no longer on them

public class Hero : MonoBehaviour {

	[SerializeField] private AudioClip JumpSound;
	[SerializeField] private AudioClip PlayerHitSound;
	[SerializeField] private bool IceEnabled = false;
	[SerializeField] private float IceMovementRate = .3f;
	[SerializeField] private float MaxXPosition = 300;

	// cached objects for performance
	private Transform t;
	private Rigidbody r;
	private BoxCollider feetCollider;

	// movement constants
	private float speed = 3f;
	private float jumpForce = 450;

	// store the maximum value of x the player has moved
	private float maxX = 0;
	private static float MAX_X_DEVIATION = 8.5f;

	// true if on ground
	private bool grounded = false;

	// cache static camera position data
	private float cameraY = 1f;
	private float cameraZ;

	// left/right direction
	private int direction = 1;

	// l/r movement value for ice movement
	private float previousH = 0;

	// default scale settings
	private float scaleX;
	private float scaleY;
	private float scaleZ;

	// set to true to disallow input
	private bool Frozen = false;

	void Start() {
		// cache components
		this.t = GetComponent<Transform> ();
		this.r = GetComponent<Rigidbody> ();
		this.feetCollider = GetComponentInChildren<HeroFeet>().GetComponent<BoxCollider> ();

		// camera z should not change - cache it now
		Vector3 cameraPos = Camera.main.transform.position;
		cameraZ = cameraPos.z;

		// set scale options
		scaleX = transform.localScale.x;
		scaleY = transform.localScale.y;
		scaleZ = transform.localScale.z;
	}

	// Performing the l/r movement on FixedUpdate prevents 
	// many of the problems with collisions with walls.
	// However, it also causes problems with jump responsiveness.
	// So, jumping is in the Update method.
	void FixedUpdate () {
		// if frozen, return
		if (Frozen) {
			return;
		}

		// get L/R movement
		float h = Input.GetAxis ("Horizontal");

		// set direction flag
		// since direction is directly based on input, this should be done before ice
		if (h > 0) {
			direction = 1;
		} else if (h < 0) {
			direction = -1;
		}

		// if ice is enabled, slow movement
		if (IceEnabled) {
			if (grounded) {
				// simulate icy floor
				if (h > previousH) {
					h = previousH + (IceMovementRate * Time.deltaTime);
				} else if (h < previousH) {
					h = previousH - (IceMovementRate * Time.deltaTime);
				}
				// check for stop
				if (previousH > 0 && h < 0) {
					h = 0;
				} else if (previousH < 0 && h > 0) {
					h = 0;
				}
				previousH = h;

			} else {
				previousH = h;
			}
		}

		// change scale to set direction
		transform.localScale = new Vector3 (scaleX * direction, scaleY, scaleZ);

		// get desired new x position
		float newX = t.position.x + (h * Time.deltaTime * speed);

		// see if it is larger than the max x value
		if (newX > maxX) {
			maxX = newX;
		}

		// calculate the minimum x value - user can't go backwards
		float minX = maxX - MAX_X_DEVIATION;

		// clamp desired min position
		newX = Mathf.Clamp (newX, minX, float.MaxValue);

		// determine new position
		Vector3 pos = new Vector3 (newX, t.position.y, t.position.z);
		t.position = pos;
	}

	void Update() {
		if (Frozen) {
			return;
		}

		// jump
		if (Input.GetButtonDown("Jump") && grounded) {
			// If we are on a moving platform, we need to disable collision detection
			// so that the player isn't automatically reparented to the platform
			// and then manually execute the code that happens when exiting the platform
			// specifically, unparent and set kinematic to false. Without doing this,
			// the user is unable to jump from a moving platform. After this is complete,
			// wait a brief period of time for the user to exit the collider of the 
			// moving platform and then renable the box collider.
			if (t.parent != null) {
				feetCollider.enabled = false;
				t.parent = null;
				r.isKinematic = false;
				StartCoroutine ("EnableBoxCollider");
			}

			// set velocity to 0 first to prevent multi-jump
			r.velocity = new Vector3(0,0,0);
			// add force to jump
			r.AddForce (new Vector3 (0, jumpForce, 0));
			grounded = false;

			AudioManager.Instance.PlayEffect(JumpSound);
		}
	}

	// reenable the box collider after a brief period
	private IEnumerator EnableBoxCollider() {
		yield return new WaitForSeconds (.1f);
		feetCollider.enabled = true;
	}
		
	void LateUpdate() {
		float playerX = t.position.x;
		float cameraX = Camera.main.transform.position.x;

		// don't let the player go back
		if (playerX > cameraX) {
			float newCameraX = Mathf.Clamp (playerX, -1, MaxXPosition);		
			Vector3 pos = new Vector3 (newCameraX, cameraY, cameraZ);
			Camera.main.transform.position = pos;
		}
	}

	// The hero object has a child collider at the feet to detect when grounded
	public void OnFeetHitGround(Collider other) {
		grounded = true;

		// if moving floor, parent this object to moving platform
		if (other.gameObject.tag == GameController.MovingFloorTag) {
			t.parent = other.transform;
			r.isKinematic = true;
		}

		// trigger any events that should happen when the player lands on this object
		other.gameObject.SendMessage ("OnCharacterLandedOn", SendMessageOptions.DontRequireReceiver);
	}

	public void OnFeetLeftGround(Collider other) {
		// if leaving moving floor, unparent
		if (other.gameObject.tag == GameController.MovingFloorTag) {
			t.parent = null;
			r.isKinematic = false;
		}

		// trigger any events that should happen when a player leaves the ground
		other.gameObject.SendMessage("OnCharacterLeft", SendMessageOptions.DontRequireReceiver);
	}

	public void OnHeadHitObject(Collider other) {
		other.gameObject.SendMessage ("OnCharacterHitFromBelow", SendMessageOptions.DontRequireReceiver);
	}

	public void OnHitByEnemy () {
		AudioManager.Instance.PlayEffect(PlayerHitSound);
		GameController.Instance.LostLife();
	}

	public void FreezePlayer () {
		Frozen = true;
	}
}
