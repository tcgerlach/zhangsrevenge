﻿using UnityEngine;
using System.Collections;

public class Pit : MonoBehaviour {

	// prevent multiple hits
	private bool hitRegistered = false;

	void OnTriggerEnter(Collider other) {
		if (!hitRegistered) {
			if (other.tag == GameController.PlayerTag) {
				GameController.Instance.LostLife ();
				// once the player is hit, don't register any more hits
				// this prevents the ocassional double hit
				hitRegistered = true;
			} else if (other.tag == GameController.EnemyTag) {
				Destroy (other.gameObject);
			}
		}
	}
}
