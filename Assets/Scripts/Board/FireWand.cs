﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWand : MonoBehaviour {

	private bool hitRegistered = false;

	void OnTriggerEnter (Collider other) {
		if (!hitRegistered) {
			if (other.tag == GameController.PlayerTag) {
				Hero player = other.GetComponent<Hero> ();
				player.OnHitByEnemy ();
				hitRegistered = true;
			}
		}
	}
}
