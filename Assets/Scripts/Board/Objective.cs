﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Objective : MonoBehaviour {

	[SerializeField] private bool IsWarp;

	private int NextBoard;			// where does this board go?
	private Canvas DisplayCanvas;	// what does it display?

	void Start () {
		LevelData Level = LevelDataHandler.Instance.GetLevelData(SceneManager.GetActiveScene().buildIndex);
		if (IsWarp) {
			DisplayCanvas = GameObject.Find ("WarpHUD").GetComponent<Canvas>();
			NextBoard = Level.WarpLevel;
		} else {
			DisplayCanvas = GameObject.Find ("WinHUD").GetComponent<Canvas>();
			NextBoard = Level.NextLevel;
		}
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.tag == GameController.PlayerTag) {
			GameObject.FindObjectOfType<Hero>().FreezePlayer();
			DisplayCanvas.enabled = true;
			Invoke("LoadNextBoard", 3);
		}
	}

	private void LoadNextBoard() {
		SceneManager.LoadScene(NextBoard);
	}
}
