﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeFlameFloor : MonoBehaviour {

	[SerializeField] private GameObject FireTrap;
	[SerializeField] private float DefaultY = -3.7f; 
	[SerializeField] private int StartX = -10;
	[SerializeField] private int EndX = 300;

	// Use this for initialization
	void Start () {
		for (int i = StartX; i < EndX; ++i) {
			Vector3 pos = new Vector3(i, DefaultY, 0);
			Instantiate (FireTrap, pos, Quaternion.identity);
		}
	}
}
