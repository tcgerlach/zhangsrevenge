﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

	[SerializeField] AudioClip CoinSoundEffect;

	// prevent multiple hits
	private bool hitRegistered = false;

	void OnTriggerEnter (Collider other) {
		if (!hitRegistered) {
			// play sound, destroy coin, increment score
			AudioManager.Instance.PlayEffect (CoinSoundEffect);
			Destroy (this.gameObject);
			GameController.Instance.IncrementScore ();
			hitRegistered = true;
		}
	}
}
