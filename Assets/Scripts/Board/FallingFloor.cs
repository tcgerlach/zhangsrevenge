﻿using UnityEngine;
using System.Collections;

public class FallingFloor : MonoBehaviour {

	private bool isFalling = false;

	// this is to be called by the hero when he hits this brick
	public void OnCharacterLandedOn() {
		if (!isFalling) {
			isFalling = true;
			StartCoroutine ("Fall");
		}
	}

	private IEnumerator Fall() {
		isFalling = true;
		yield return new WaitForSeconds (.15f);
		Rigidbody r = this.GetComponent<Rigidbody> ();
		r.isKinematic = false;
		r.useGravity = true;
	}
}
