# Zhang's Revenge #

This project is a platformer demo game from the **Altoona Game Development Club** written using Unity.

Use arrow keys to move Zhang  
Space key to jump  

To go to any board you want at any time, click on the corresponding key:  

0: Opening Scene  
1: Board 1 (By Tom)  
2: Board 2 (By Alanna)  
3: Board 3 (By Ron)  
4: Board 4 (By Alanna)  
5: Board 4 (Ice board, by Tom)  
6: Board 5 (Dungeon, by Tom)  
7: Credits  

M to mute audio  
ESC to pause  
